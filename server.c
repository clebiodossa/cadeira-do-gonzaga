#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <pcap.h>
#include <assert.h>

// pacakges definition
#define SNAP_LEN 1518
#define SIZE_ETHERNET 14


struct sniff_ip {
        u_char  ip_vhl;                 /* version << 4 | header length >> 2 */
        u_char  ip_tos;                 /* type of service */
        u_short ip_len;                 /* total length */
        u_short ip_id;                  /* identification */
        u_short ip_off;                 /* fragment offset field */
        #define IP_RF 0x8000            /* reserved fragment flag */
        #define IP_DF 0x4000            /* dont fragment flag */
        #define IP_MF 0x2000            /* more fragments flag */
        #define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
        u_char  ip_ttl;                 /* time to live */
        u_char  ip_p;                   /* protocol */
        u_short ip_sum;                 /* checksum */
        struct  in_addr ip_src,ip_dst;  /* source and dest address */
};
#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

// TCP header
typedef u_int tcp_seq;

struct sniff_tcp {
        u_short th_sport;               /* source port */
        u_short th_dport;               /* destination port */
        tcp_seq th_seq;                 /* sequence number */
        tcp_seq th_ack;                 /* acknowledgement number */
        u_char  th_offx2;               /* data offset, rsvd */
#define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)
        u_char  th_flags;
        #define TH_FIN  0x01
        #define TH_SYN  0x02
        #define TH_RST  0x04
        #define TH_PUSH 0x08
        #define TH_ACK  0x10
        #define TH_URG  0x20
        #define TH_ECE  0x40
        #define TH_CWR  0x80
        #define TH_FLAGS        (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
        u_short th_win;                 /* window */
        u_short th_sum;                 /* checksum */
        u_short th_urp;                 /* urgent pointer */
};


pthread_t tid[2]; //thread
int ret1;  //thread control
int RC[4]; //return code sniffer thread
char RCOpFTrace[1024]; //return code OpFTrace

// SNIFFER method
char* ip_list[1];				/* array with IPs */
int ip_list_len=1;  			/* number of IPs */
int count = 1;                  /* packet counter */

/* FUNCTION order array                   */
/* Count distinct IPs from ip_list        */
/* RETURN the sum of most frequent IP or 0*/
int order_array()
{

    // if there is IPs in the ip_list
	if(ip_list_len>2)
    {
      int j=0, caifora=0, k=0, i=0, z=0, x, big=0;
      char* temp[1];           /* temp array with distinct IPs */
      char RCA[20], RCA1[3];   /* array with most frequent IP */
		
	  // initialize array with IP most frequent	
      for(j=0;j<20;j++) 
      {
          RCA[j]='\0';
      }

      // allocate memory to temp array
      temp[k] = (char*)malloc(sizeof(char)*sizeof(ip_list[0]));        
	  // add first IP to temp array
      strcpy(temp[k],ip_list[0]);
      k++;;
    
      // add distinct IPs to temp array
	  for(j=0;j<=ip_list_len;j++)
      {
        caifora=0;

        for(i=0; i<k;i++)
        {
          if(strcmp(temp[i],ip_list[j]) == 0)
          {
            caifora=1;   
            break;
          }     
        }
		// caifora false means the IP is distinct, add to temp array
        if(caifora==0)
        {    
		  // allocate memory to temp array		
          temp[k] = (char*)malloc(sizeof(char)*sizeof(ip_list[j])); 
		  // add first IP to temp array
          strcpy(temp[k],ip_list[j]);
          k++;
        }
      }
    
      // count the most frequent IP to RCA and big receive the number of times
      int clone=0;
      for(z=0;z<k;z++)
      {        
          for(j=0;j<=ip_list_len;j++)
          {
              if(strcmp(temp[z],ip_list[j]) == 0)
              {
                  clone++;   
//                  printf("IP %s = %d\n",temp[z],clone);
              }
              if(clone>big)
              {
                  // big receive the sum of most frequent
				  big=clone;
				  // copy the most frequent to RCA
                  strcpy(RCA,temp[z]);
//                  printf("\ncopy %s para RCA \n",temp[z]);    
              }              
          }
          clone=0;
      }        
      printf("\n[%d][ordering] IP %s with %d times\n",getpid(),RCA,big);
      

        // copy the most frequent to the return code RC that will be delivered to client
		j=0,z=0;x=0;
        while(RCA[j]!='\0')
        {
            if(RCA[j]!='.')
            {
                RCA1[z++]=RCA[j];
            }
            else
            {
                RCA1[z]='\0';
                RC[x]=atoi(RCA1);
                x++;
                strcpy(RCA1,"");
                z=0;
            }
            j++;
        }
        RCA1[j]='\0';
        RC[x]=atoi(RCA1);        


      //free resources: allocated temp and allocated ip_list heap memory
       for(j=0;j<=ip_list_len;j++)
       {
           free(ip_list[j]);
       }          
       ip_list_len=1;
       for(z=0;z<k;z++)
       {
           free(temp[z]);
       }     
       count=1;
        
       return big; 
    }        
    return 0;
}


/* FUNCTION got_packet                  				*/
/* Capture TCP package and add to ip_list  				*/
/* promiscuous mode                            				*/
/* Call got_packet where the ip_list is filled 				*/
/* Call order_array where the most frequent is determined 		*/
/* RETURN NULL								*/
/*									*/
/* Copyright (c) 2005 The Tcpdump Group					*/
/************************************************************************/
void got_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
   
    const struct sniff_ip *ip;              /* The IP header */
    const struct sniff_tcp *tcp;            /* The TCP header */

    int size_ip;
    int size_tcp;
    
    printf("%d, ", count);
    count++;

    ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    if (size_ip < 20) {
	printf("[%d][sniffer][ERROR] Invalid IP header length: %u bytes\n", getpid(),size_ip);
	return;
    }
//    printf("[%d]       From: %s\n", getpid(),inet_ntoa(ip->ip_src));
//    printf("[%d]         To: %s\n", getpid(), inet_ntoa(ip->ip_dst));

    tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
    size_tcp = TH_OFF(tcp)*4;
    if (size_tcp < 20) {
		printf("[%d][sniffer][ERROR] Invalid TCP header length: %u bytes\n", getpid(),size_tcp);
		return;
    }

	// allocate memory to array ip_list
    ip_list[count-2] = (char*)malloc(sizeof(char)*size_ip);    
	// copy the source IP to the array
    strcpy(ip_list[count-2],inet_ntoa(ip->ip_src));
	// increment number of IPs
    ip_list_len=count-2;

    return;
}

/* FUNCTION OpFTrace thread						*/
/* Run in a thread and enable interface to     				*/
/* promiscuous mode                            				*/
/* Call got_packet where the ip_list is filled 				*/
/* Call order_array where the most frequent is determined 		*/
/* RETURN NULL								*/
/************************************************************************/
void* OpFTrace_thread(void *arg)
{
    printf("[%d] Initializing OpFTrace..\n",getpid());
    FILE *fp;
    char path[1024];
    
    /* Open the command for reading. */
    fp = popen("tail -n1 /var/log/openvswitch/ovs-vswitchd.log", "r");
    if (fp == NULL) {
	fprintf(stderr, "[%d][ERROR] Failed to read log file\n", getpid());
        exit(1);
    }
                    
    /* Read the output a line at a time - output it. */
    while (fgets(path, sizeof(path)-1, fp) != NULL) {
//     printf("%s", path);
       strcpy(RCOpFTrace,path);
    }
    pclose(fp);

    // return to thread requester
    pthread_exit(&ret1);
    
    return NULL;    
}


/* FUNCTION sniffer thread                     				*/
/* Run in a thread and enable interface to     				*/
/* promiscuous mode                            				*/
/* Call got_packet where the ip_list is filled 				*/
/* Call order_array where the most frequent is determined 		*/
/* RETURN NULL								*/
 /* pcap instructions: Copyright (c) 2005 The Tcpdump Group		*/
/************************************************************************/
void* sniffer_thread(void *arg)
{
    char *dev = NULL;		/* capture device name */
    char errbuf[PCAP_ERRBUF_SIZE];	/* error buffer */
    pcap_t *handle;		/* packet capture handle */
    char filter_exp[] = "ip";	    /* filter expression [3] */
    struct bpf_program fp;	    /* compiled filter program (expression) */
    bpf_u_int32 mask;		/* subnet mask */
    bpf_u_int32 net;		/* ip */
    int num_packets = 100;	    /* number of packets to capture */


    printf("[%d] Initializing sniffer..\n",getpid());
        
    dev = pcap_lookupdev(errbuf);
    if (dev == NULL) {
        fprintf(stderr, "[%d] Couldn't find default device: %s\n",getpid(),errbuf);
        exit(EXIT_FAILURE);
    }
    if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
	fprintf(stderr, "[%d] Couldn't get netmask for device %s: %s\n",getpid(),dev, errbuf);
	net = 0;
	mask = 0;
    }

    printf("[%d] Device: %s\n", getpid(),dev);
    printf("[%d] Number of packets: %d\n",getpid(), num_packets);
    printf("[%d] Filter expression: %s\n",getpid(), filter_exp);
    
    // open capture device 
    handle = pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
    if (handle == NULL) {
	fprintf(stderr, "[%d][ERRROR] Couldn't open device %s: %s\n", getpid(),dev, errbuf);
	exit(EXIT_FAILURE);
    }

    // make sure we're capturing on an Ethernet device [2] 
    if (pcap_datalink(handle) != DLT_EN10MB) {
	fprintf(stderr, "[%d][ERROR] %s is not an Ethernet\n", getpid(),dev);
	exit(EXIT_FAILURE);
    }

    // compile the filter expression 
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
	fprintf(stderr, "[%d][ERROR] Couldn't parse filter %s: %s\n",getpid(),filter_exp, pcap_geterr(handle));
	exit(EXIT_FAILURE);
    }

    // apply the compiled filter 
    if (pcap_setfilter(handle, &fp) == -1) {
	fprintf(stderr, "[%d]{ERROR] Couldn't install filter %s: %s\n",getpid(),filter_exp, pcap_geterr(handle));
	exit(EXIT_FAILURE);
    }

    // now we can set our callback function 
    pcap_loop(handle, num_packets, got_packet, NULL);
	
	// determine the most frequent	
    ret1=order_array();
	
	// return to thread requester
    pthread_exit(&ret1);
    
    return NULL;
}



/* FUNCTION new_thread                    				*/
/* Starts a new thread as per the request received 		*/
/* RECEIVE the method to be triggered      				*/
/* RETURN integer						   				*/
int new_thread(int x)
{
   int err;
   int *ptr[1];

   /* METHODS availables */
   /* 1 = SNIFFER method */
   /* 2 = OpFTrace	 */
   /* 3 = ....			 */
   /* 4 = ....			 */
   switch (x)
   {
     case 1:
       printf ("[%d] Running Sniffer Thread..\n",getpid());
       err = pthread_create(&(tid[0]), NULL, &sniffer_thread, NULL);
       if (err != 0)
            printf("[%d][ERROR] can't create thread :[%s]",getpid(), strerror(err));

       pthread_join(tid[0], (void**)&(ptr[0]));
       printf("[%d] Return code sniffer thread [%d]\n", getpid(),*ptr[0]);
       
       return *ptr[0];
       
     break;
     case 2:
      printf ("[%d] Running OpFTrace Thread..\n",getpid());
       err = pthread_create(&(tid[0]), NULL, &OpFTrace_thread, NULL);
       if (err != 0)
            printf("[%d][ERROR] can't create thread :[%s]",getpid(), strerror(err));

       pthread_join(tid[0], (void**)&(ptr[0]));
       printf("[%d] Return code OpFTrace thread [%d]\n", getpid(),*ptr[0]);
       
       return *ptr[0];
      
       return 0;
     break;
     default:
      return x;
    }   
}

/* FUNCTION main               								 */
/* Start listener	 											*/
/* Start the method requested to be executed in a new thread	*/
int main(void)
{
    int listenfd = 0, connfd = 0, n = 0;
    struct sockaddr_in serv_addr; 
     
    char sendBuff[1025];
    char recvBuff[1024];
    int RC_QTD = 0;
//    char buffer[sizeof(RC)+sizeof(RC_QTD)];
    char buffer[1024];
             
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff)); 
                         
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000);   /* TCP port */
                                     
    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 
    listen(listenfd, 10); 

    printf("\n[LISTENER] Waiting data from socket..\n");
	
    // loop in listener
    while(1)
    {
       // listen in port 
	   connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
       // read from socket     
       while ( (n = read(connfd, recvBuff, sizeof(recvBuff)-1)) > 0)
       {
            recvBuff[n] = 0;
            if(fputs(recvBuff, stdout) == EOF)
            {
                printf("\n Error : puts error\n");
            }            
			// run method sniffer
            if(strncmp("sniffer", recvBuff, sizeof("sniffer")) == 0)
            {
                printf("\n[LISTENER] Running function:  %s \n",recvBuff);
				// start new thread with sniffer method (1)
                RC_QTD = new_thread(1);
                //new_thread(1);
                sprintf(buffer,"%d.%d.%d.%d %d",RC[0],RC[1],RC[2],RC[3],RC_QTD);                
                break;                
            }
            else if(strncmp("OpFTrace", recvBuff, sizeof("OpFTrace")) == 0)
            {
                printf("\n[LISTENER] Running function:  %s \n",recvBuff);
                RC_QTD = new_thread(2);
                strcpy(buffer,RCOpFTrace);                
                break;
            }
            // method not identified, return not found to client
            else
            {
                printf("\n[LISTENER] Unknown function %s\n",recvBuff);
                strcpy(buffer,"not found");
                break;
            }
       }                                                        

	   // sending return to client
       printf("\n..Sending return.. %s\n",buffer);
       write(connfd, buffer, strlen(buffer)); 
       close(connfd);	   
       sleep(1);
    }
    
    return 0;
}
