Project Name: A server to run predefined methods

Group: Clébio Dossa

Summary: 
	
	The server will bind a TCP port with a listener main thread and
	run predefined methods as per a client request.
	The method will be validated if properly called and the listener
	trigger a new thread to run the desired method.

Goals:

	Monitor OS specific informations to return to the requester (client).
	The methods should explore distinct information from OS level in order
	to keep a remote client updated about its OS status.
	
Details:

	Methods:

	1 - SNIFFER:
		Method to capture 100 packets and return the most frequent source
		address IP
	2 - OpFTrace:
                Method to run the last line of log file of openvswtich
	3 -

Build:

        server
        gcc -pthread -Wall -o server server.c -lpcap

        client
        gcc- o client client.c

Dependencies:

       general with done with build-essential
       specific pthreads and libpcap